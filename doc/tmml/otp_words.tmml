




<!-- Generated from file 'otp_words.man' by tcllib/doctools with format 'tmml' -->
<manpage id='otp_words' cat='cmd' title='otp_words' version='2.1.3' package=''>
<head>
<info key='copyright' value='Copyright (c) 1996-2003, Andreas Kupries &lt;andreas_kupries@users.sourceforge.net&gt;'/>
</head>
<namesection>
<name>otp_words</name>
<desc>Encoding "otp_words"</desc>

</namesection>








<synopsis>
<syntax>
package require <package>Tcl</package> <o>8.2</o>
package require <package>Trf</package> <o>2.1.3</o>
<cmd>otp_words</cmd> <o><m>options...</m></o> <o><m>data</m></o>
</syntax>
</synopsis>
<section id='section1'>
<title>DESCRIPTION</title>

The command <cmd>otp_words</cmd> is one of several data encodings
provided by the package <package>trf</package>. See <syscmd>trf-intro</syscmd> for an
overview of the whole package.



<p>

This encoding transforms every block of four bytes (64 bit) into six
english words, as defined in

RFC 2289 (<url>http://www.rfc-editor.org/rfc/rfc2289.txt</url>).

This encoding is the sole one which is <emph>not able</emph> to handle an
incomplete block at the end of the input.


</p>
<p>
</p>
<dl>


<dle>
<dt><cmd>otp_words</cmd> <o><m>options...</m></o> <o><m>data</m></o></dt>
<dd>


<dl>



<dle>
<dt><option>-mode</option> <l>encode</l>|<l>decode</l></dt>
<dd>

This option has to be present and is always understood by the
encoding.

<br/>

For <term>immediate</term> mode the argument value specifies the operation
to use.  For an <term>attached</term> encoding it specifies the operation to
use for <emph>writing</emph>. Reading will automatically use the reverse
operation.

See section <ref refid='section2'>IMMEDIATE versus ATTACHED</ref> for explanations of
these two terms.

<br/>

Beyond the argument values listed above all unique abbreviations are
recognized too.

<br/>

<l>Encode</l> converts from arbitrary (most likely binary) data into
the described representation, <l>decode</l> does the reverse .



</dd>
</dle>
<dle>
<dt><option>-attach</option> <m>channel</m></dt>
<dd>

The presence/absence of this option determines the main operation mode
of the transformation.

<br/>

If present the transformation will be stacked onto the <m>channel</m>
whose handle was given to the option and run in <term>attached</term>
mode. More about this in section <ref refid='section2'>IMMEDIATE versus ATTACHED</ref>.

<br/>

If the option is absent the transformation is used in <term>immediate</term>
mode and the options listed below are recognized. More about this in
section <ref refid='section2'>IMMEDIATE versus ATTACHED</ref>.


</dd>
</dle>
<dle>
<dt><option>-in</option> <m>channel</m></dt>
<dd>

This options is legal if and only if the transformation is used in
<term>immediate</term> mode. It provides the handle of the channel the data
to transform has to be read from.

<br/>

If the transformation is in <term>immediate</term> mode and this option is
absent the data to transform is expected as the last argument to the
transformation.


</dd>
</dle>
<dle>
<dt><option>-out</option> <m>channel</m></dt>
<dd>

This options is legal if and only if the transformation is used in
<term>immediate</term> mode. It provides the handle of the channel the
generated transformation result is written to.

<br/>

If the transformation is in <term>immediate</term> mode and this option is
absent the generated data is returned as the result of the command
itself.

</dd>
</dle>

</dl>
</dd>
</dle>

</dl>







</section>
<section id='section2'>
<title>IMMEDIATE VERSUS ATTACHED</title>

The transformation distinguishes between two main ways of using
it. These are the <term>immediate</term> and <term>attached</term> operation
modes.


<p>

For the <term>attached</term> mode the option <option>-attach</option> is used to
associate the transformation with an existing channel. During the
execution of the command no transformation is performed, instead the
channel is changed in such a way, that from then on all data written
to or read from it passes through the transformation and is modified
by it according to the definition above.

This attachment can be revoked by executing the command <cmd>unstack</cmd>
for the chosen channel. This is the only way to do this at the Tcl
level.

</p>
<p>

In the second mode, which can be detected by the absence of option
<option>-attach</option>, the transformation immediately takes data from
either its commandline or a channel, transforms it, and returns the
result either as result of the command, or writes it into a channel.

The mode is named after the immediate nature of its execution.

</p>
<p>

Where the data is taken from, and delivered to, is governed by the
presence and absence of the options <option>-in</option> and <option>-out</option>.

It should be noted that this ability to immediately read from and/or
write to a channel is an historic artifact which was introduced at the
beginning of Trf's life when Tcl version 7.6 was current as this and
earlier versions have trouble to deal with \0 characters embedded into
either input or output.




</p>
</section>
<seealso>
<ref>trf-intro</ref>
<ref>md5_otp</ref>
<ref>trf-intro</ref>
<ref>ascii85</ref>
<ref>base64</ref>
<ref>bin</ref>
<ref>hex</ref>
<ref>oct</ref>
<ref>otp_words</ref>
<ref>quoted-printable</ref>
<ref>uuencode</ref>
</seealso>
<keywords>
<keyword>rfc 2289</keyword>
<keyword>md5_otp</keyword>
<keyword>otp_words</keyword>
<keyword>encoding</keyword>
</keywords>

</manpage>

