'\"
'\" Generated from file 'crypt.man' by tcllib/doctools with format 'nroff'
'\" Copyright (c) 1996-2003, Andreas Kupries <andreas_kupries@users.sourceforge.net>
'\"
.so man.macros
.TH "crypt" n 2.1.3  "Trf transformer commands"
.BS
.SH NAME
crypt \- Password hashing based on "crypt"
.SH SYNOPSIS
package require \fBTcl  ?8.2?\fR
.sp
package require \fBTrf  ?2.1.3?\fR
.sp
\fBcrypt\fR \fIpassword\fR \fIsalt\fR
.sp
.BE
.SH DESCRIPTION
The command \fBcrypt\fR is an interface to the \fBcrypt(3)\fR
function for the encryption of passwords. An alternative command for
the same, but based on \fImd5\fR is \fBmd5crypt\fR.
.PP
.TP
\fBcrypt\fR \fIpassword\fR \fIsalt\fR
Encrypts the \fIpassword\fR using the specified \fIsalt\fR and returns
the generated hash value as the result of the command.
.PP
.SH "SEE ALSO"
md5crypt, trf-intro
.SH KEYWORDS
authentication, crypt, hash, hashing, mac, md5, message digest, password
.SH COPYRIGHT
.nf
Copyright (c) 1996-2003, Andreas Kupries <andreas_kupries@users.sourceforge.net>

.fi