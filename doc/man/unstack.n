'\"
'\" Generated from file 'unstack.man' by tcllib/doctools with format 'nroff'
'\" Copyright (c) 1996-2003, Andreas Kupries <andreas_kupries@users.sourceforge.net>
'\"
.so man.macros
.TH "unstack" n 2.1.3  "Trf transformer commands"
.BS
.SH NAME
unstack \- Unstacking channels
.SH SYNOPSIS
package require \fBTcl  ?8.2?\fR
.sp
package require \fBTrf  ?2.1.3?\fR
.sp
\fBunstack\fR \fIchannel\fR
.sp
.BE
.SH DESCRIPTION
The command \fBunstack\fR is an interface to the public Tcl API
function \fBTclUnstackChannel\fR. It unstacks the topmost
transformation from the specified channel if there is any.
.PP
.TP
\fBunstack\fR \fIchannel\fR
Removes the topmost transformation from the specified
\fIchannel\fR. If the \fIchannel\fR has no transformation associated
with it it will be closed. In other words, in this situation the
command is equivalent to \fBclose\fR.
.PP
.SH "SEE ALSO"
trf-intro
.SH KEYWORDS
removal, transformation, unstacking
.SH COPYRIGHT
.nf
Copyright (c) 1996-2003, Andreas Kupries <andreas_kupries@users.sourceforge.net>

.fi