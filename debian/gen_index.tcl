#!/usr/bin/tclsh

set docdir [lindex $argv 0]
set cdocdir [lindex $argv 1]
set manuals {}
set capi_manuals {}

foreach docfile [glob -nocomplain -directory $docdir *.html] {
    puts stderr $docfile

    if {[catch {set fd [open $docfile]} msg]} {
	puts stderr "Can't open file $docfile: $msg"
	continue
    }

    while {[gets $fd line] >= 0} {
	if {[regexp {<h1.*>\s*(.*)\(3trf\)\s+(\S+)\s+.*</h1>} $line -> \
		    name version]} {
	}
	if {[regexp {<p>\s*([^-]*)\s*-\s*(.*)} $line -> - title]} {
	    lappend manuals [list $name $name $version $title [file tail $docfile]]
	    break
	}
    }
    close $fd
}

foreach docfile [glob -nocomplain -directory $cdocdir *.html] {
    puts stderr $docfile

    if {[catch {set fd [open $docfile]} msg]} {
	puts stderr "Can't open file $docfile: $msg"
	continue
    }

    while {[gets $fd line] >= 0} {
	if {[regexp {<title>\s*(\S*)\s*-\s*(.*?)\.?</title>} $line -> \
		    name title]} {
	    lappend capi_manuals [list $name $name $title [file tail $docfile]]
	    break
	}
    }
    close $fd
}

puts "<html>
<head>
<title>Tcl Trf HTML Documentation</title>
<style type=\"text/css\">
<!--
ul {
    background: lightyellow;
    border-style: solid;
    border-width: 1px;
    border-color: black;
}
li {
    padding: 3px;
}
li a {
    font-weight: bold;
}
-->
</style>
</head>
<body>
<h1>Tcl Trf HTML Documentation</h1>
<h2>Trf transformer commands</h2>
<ul>"

set manuals [lsort -index 0 $manuals]
foreach m $manuals {
    set name    [lindex $m 1]
    set version [lindex $m 2]
    set title   [lindex $m 3]
    set fname   [lindex $m 4]

    puts "<li><a href=\"$fname\">$name</a> ($version): $title</li>"
}
puts "</ul>
<h2>Trf C API</h2>
<ul>"

set manuals [lsort -index 0 $capi_manuals]
foreach m $manuals {
    set name    [lindex $m 1]
    set title   [lindex $m 2]
    set fname   [lindex $m 3]

    puts "<li><a href=\"$fname\">$name</a>: $title</li>"
}

puts "</ul>
</body>
</html>"

